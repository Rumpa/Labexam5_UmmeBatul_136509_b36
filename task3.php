<?php
class Calculator{
    public $fval;
    public $sval;

    public function __construct($fval,$sval)
    {
        $this->fval=$fval;
        $this->sval=$sval;
    }
    public function add(){
        return $this->fval+$this->sval;
    }
    public function subtract(){
        return $this->fval-$this->sval;
    }
    public function multiply(){
        return $this->fval*$this->sval;
    }
    public function divide(){
        return $this->fval/$this->sval;
    }
}
$mycalc=new Calculator(12,6);
echo "Addition:".$mycalc->add()."<br>";
echo "Subtraction:".$mycalc->subtract()."<br>";
echo "Multiplication:".$mycalc->multiply()."<br>";
echo "Division:".$mycalc->divide();

?>